from app import app, Blueprint, request, models, db, jsonify, jwt_required, config
from app.models.User import User

auth = Blueprint('auth', __name__)

@auth.route('register',  methods=['POST'])
def register():
	name 		= request.json['name']
	email 		= request.json['email']	
	password 	= request.json['password']
	checkUser 	= User.query.filter_by(email=email).first()

	if not checkUser:
		newUser = User(name=name,email=email,password=password)
		db.session.add(newUser)
		db.session.commit()
		newUser.create_wallet()
		
		db.session.commit()
		return jsonify({"status" : "success", "token" : newUser.generate_jwt(), "data" : newUser.as_dict()}), 200
	else:
		return jsonify({"status" : "error", "message" : "Email has already been registered"}), 403


@auth.route('login', methods=['POST'])
def login():
	email 		= request.json['email']	
	password 	= request.json['password']

	checkUser 	= User.query.filter_by(email=email).first()

	if checkUser and checkUser.verify_password(password):
		return jsonify({"status" : "success", "message" : "login successful", "token" : checkUser.generate_jwt(), "user" : checkUser.as_dict()})
	else:
		return jsonify({"status" : "error", "message" : "Username or password incorrect"}), 400


