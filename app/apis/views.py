from app import app, Blueprint, request, db, jsonify, jwt_required, config, decode_token, g, Repo
from app.models.User import User

from app.Repo import User as UserRepo
from app.Repo import Wallet as WalletRepo
from app.Repo import Transaction as TransactionRepo
from app.Repo import Reward as RewardRepo
import random, string, requests, datetime

api = Blueprint('api', __name__)

#wrapper to exclude certain routes from token checks
def exclude_from_token_check(func):
	func._exclude_from_token_check = True
	return func


@api.before_request
def token_check(*args, **kwargs):
	# this method was running even when the request was 'options' disrupting normal app flow
	check_token = False
	if request.endpoint in app.view_functions:
	    view_func = app.view_functions[request.endpoint]
	    check_token = not hasattr(view_func, '_exclude_from_token_check')

	if request.method != 'OPTIONS' and check_token:
		token 			= request.headers.get('Authorization').replace('Bearer ','')
		public_id 		= decode_token(token)['identity']
		if public_id:
			user = User.query.filter_by(public_id=public_id).first()
			if user:
				g.user = user
		else:
			return jsonify({"status" : "error", "message" : "unauthorized, please login again"}), 403

#Method returns a logged in user's details
@api.route('/user/')
def get_user_details():
	return jsonify({"status":"success", "data":g.user.as_dict(), "message":"Profile retrieved successfully"}), 200


#Retrieve profile of other users by ID
@api.route('/user/<user_id>/')
def get_other_user_details(user_id):

	user = User.query.filter_by(public_id=user_id).first()
	if user:
		return jsonify({"status":"success", "message":"User details retrieved successfully", "data":user.as_dict()}), 200
	else:
		return jsonify({"status":"error", "message":"user was not found"}), 404

#List all Apps a User has
@api.route('/user/app/')
def get_user_apps():
	all_apps = UserRepo.get_app_list(g.user.id)
	return jsonify({"status":"success", "data":all_apps}),200


#Save users' Apps
@api.route('/user/app/', methods=['POST'])
def upload_user_apps():
	app_list = request.json.get('apps')
	if app_list is None:
		return jsonify({"status":"error", "message":"App list is empty"}), 400

	set_apps = UserRepo.add_app(g.user.id,app_list)
	if set_apps:
		return jsonify({"status":"success", "message":"App(s) Uploaded successfully"})
	else:
		return jsonify({"status":"error", "message":"Oops an error occurred, please try again"}), 500


#Save users' Contacts
@api.route('/user/contacts/', methods=['POST'])
def upload_user_contacts():
	contact_list = request.json.get('contacts')
	if contact_list is None:
		return jsonify({"status":"error", "message":"contact list is empty"}), 400

	set_contacts = UserRepo.add_contacts(g.user.id,contact_list)
	if set_contacts:
		return jsonify({"status":"success", "message":"Contact(s) Uploaded successfully"})
	else:
		return jsonify({"status":"error", "message":"Oops an error occurred, please try again"}), 500


#Retrieve Wallet info of user
@api.route('/user/wallet/')
def get_user_wallet():
	wallet = WalletRepo.get_wallet(g.user.id)
	return jsonify({"data":wallet, "status":"success"}), 200


#Update user details
@api.route('/user/', methods=['PUT'])
def update_user_details():
	data 	= request.get_json();
	update 	= UserRepo.update_user(g.user.id, data)
	
	return jsonify({"status" : "success", "message" : "Updated Successfully", "data" : g.user.as_dict()} )


#Get List of Rewards
@api.route('/rewards/')
def get_rewards():
	rewards = RewardRepo.get_rewards()
	return jsonify({"status" : "success", "message" : "Fetch Rewards Succcessful", "data" : rewards} )


#Retrieve PArticular reward
@api.route('/rewards/<id>')
def get_reward(id):
	reward = RewardRepo.get_reward(id)
	return jsonify({"status" : "success", "message" : "Fetch Reward Succcessful", "data" : reward})


#Retrieve a users' transaction list
@api.route('/user/transactions')
def get_transactions():
	transactions = TransactionRepo.get_transactions(g.user.id)
	return jsonify({"status" : "success", "message" : "Fetch Transactions Succcessful", "data" : transactions})


@api.route('/user/transactions', methods=['POST'])
def add_transaction():
	data = request.get_json()
	save = TransactionRepo.add_transaction(g.user.id,data)
	if save:
		return jsonify({"status" : "success", "message" : "Successfully saved transaction", "data" : TransactionRepo.get_transactions(g.user.id)})
	return jsonify({"status" : "error", "message": "an error occurred"}), 400