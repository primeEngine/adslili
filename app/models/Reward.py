from . import db
import datetime

class Reward(db.Model):
	__tablename__ 	= 'rewards'
	id 				= db.Column(db.Integer, primary_key=True)
	reward 			= db.Column(db.String(500))
	description 	= db.Column(db.String(1000))
	cost 			= db.Column(db.REAL, default=1)
	active 			= db.Column(db.Boolean, default=True)
	created_at 		= db.Column(db.DateTime, default=datetime.datetime.utcnow())

	def as_dict(self):
	    return {c.name: str(getattr(self, c.name)) for c in self.__table__.columns}
