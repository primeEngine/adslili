import os, sys
basedir = os.path.abspath(os.path.dirname(__file__))
sys.path.append(os.path.join(basedir, '../'))
from app import db, create_access_token

from werkzeug.security import generate_password_hash, check_password_hash
