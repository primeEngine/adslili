from . import db,generate_password_hash, create_access_token, check_password_hash
from .Wallet import Wallet
from . import AppList, ContactList
import uuid

class User(db.Model):
	__tablename__ 			= "users"
	id 				        = db.Column(db.Integer, primary_key = True)
	public_id 		        = db.Column(db.String(100), unique=True)
	email 			        = db.Column(db.String(120), unique=True)
	name 			        = db.Column(db.String(200))
	password_hash 	        = db.Column(db.String(100))
	phone 			        = db.Column(db.String(15))
	latitude 				= db.Column(db.String(20))
	longitude 				= db.Column(db.String(20))
	status 			        = db.Column(db.Boolean, default=False)
	wallet                  = db.relationship('Wallet', backref='owner', lazy='dynamic')
	apps 					= db.relationship('AppList', backref='owner', lazy='dynamic')
	contacts 				= db.relationship('ContactList', backref='owner', lazy='dynamic')

	def generate_jwt(self):
		return create_access_token(self.public_id)

	def set_uuid(self):
		self.public_id = str(uuid.uuid4())

	@property
	def password(self):
		return 'password is not readable'

	@password.setter
	def password(self, password):
		self.password_hash = generate_password_hash(password)
		self.set_uuid()

	@property
	def is_active(self):
		return self.status

	def verify_password(self, password):
		return check_password_hash(self.password_hash, password)

	def create_wallet(self):
		new_wallet = Wallet(user_id = self.id)
		db.session.add(new_wallet)
		db.session.commit()

	def as_dict(self):
		userData = dict()
		userData['name'] 			= self.name
		userData['id'] 				= self.public_id
		userData['phone']			= self.phone
		userData['status']			= self.status
		userData['email'] 			= self.email
		userData['longitude'] 		= self.longitude
		userData['latitude'] 		= self.latitude
		return userData