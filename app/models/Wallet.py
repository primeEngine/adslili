from . import db
from . import User
import datetime

class Wallet(db.Model):
    __tablename__   = 'wallets'
    balance         = db.Column(db.REAL, default=0)
    id              = db.Column(db.Integer, primary_key=True)
    user_id         = db.Column(db.Integer,db.ForeignKey('users.id'))
    prev_balance    = db.Column(db.REAL, default=0)
    updated         = db.Column(db.DateTime, default=datetime.datetime.utcnow())
    created         = db.Column(db.DateTime, default=datetime.datetime.utcnow())

    def repr(self):
        return self.owner_id

    def increment(self, value):
        self.prev_balance 	= self.balance
        self.balance 		= self.balance + float(value)
        self.updated		= datetime.datetime.utcnow()
        return True

    def decrease(self, value):
        self.prev_balance 	= self.balance

    def can_withdraw(self, value):
        if(self.balance > float(value)):
            return True
        else:
            return False

    def as_dict(self):
        return {c.name: str(getattr(self, c.name)) for c in self.__table__.columns}
