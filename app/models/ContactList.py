from . import db
from . import User
import datetime

class ContactList(db.Model):
	__tablename__ 	= 'contact_lists'
	id 				= db.Column(db.Integer, primary_key=True)
	user_id 		= db.Column(db.Integer, db.ForeignKey('users.id'))
	name 			= db.Column(db.String(1000))
	phone			= db.Column(db.String(20))
	email			= db.Column(db.String(200))
	created_at 		= db.Column(db.DateTime, default=datetime.datetime.utcnow())

	def as_dict(self):
	    return {c.name: str(getattr(self, c.name)) for c in self.__table__.columns}