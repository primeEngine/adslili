from . import db
from . import User
import datetime

class Transaction(db.Model):
	__tablename__ 	= 'transactions'
	id 				= db.Column(db.Integer, primary_key=True)
	user_id 		= db.Column(db.Integer, db.ForeignKey('users.id'))
	other_id 		= db.Column(db.Integer)
	txn_type		= db.Column(db.String(20))
	created_at 		= db.Column(db.DateTime, default=datetime.datetime.utcnow())

	def as_dict(self):
	    return {c.name: str(getattr(self, c.name)) for c in self.__table__.columns}