from . import db
from . import User
import datetime

class AppList(db.Model):
	__tablename__ 	= 'app_lists'
	id 				= db.Column(db.Integer, primary_key=True)
	user_id 		= db.Column(db.Integer, db.ForeignKey('users.id'))
	app_name 		= db.Column(db.String(1000))
	created_at 		= db.Column(db.DateTime, default=datetime.datetime.utcnow())

	def as_dict(self):
	    return {c.name: str(getattr(self, c.name)) for c in self.__table__.columns}