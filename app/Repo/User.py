from app.models.AppList import AppList
from app.models.ContactList import ContactList
from app.models.User import User
from app import db

def get_app_list(user_id):
	all_apps = []
	apps = AppList.query.filter_by(user_id=user_id).all()

	for app in apps:
		all_apps.append(app.as_dict())

	return all_apps

def add_app(user_id, apps):
	if isinstance(apps,(list,)):
		for app in apps:
			new_app = AppList(user_id=user_id, app_name=app)
			db.session.add(new_app)
	else:
		new_app = AppList(user_id=user_id, app_name=apps)
		db.session.add(new_app)

	db.session.commit()

	return True


#add Contacts to the user
def add_contacts(user_id, contacts):
	if isinstance(contacts,(list,)):
		for contact in contacts:
			email 		= contact['email'] if 'email' in contact else '' 
			new_contact = ContactList(user_id=user_id, name=contact['name'], phone=contact['phone'], email=email)
			db.session.add(new_contact)
	else:
		email 		= contacts['email'] if 'email' in contacts else '' 
		new_contact = ContactList(user_id=user_id, name=contacts['name'], phone=contacts['phone'], email=email)
		db.session.add(new_contact)

	db.session.commit()

	return True


#Method used to update user details
def update_user(user_id, data):
	keys 			= data.keys()
	excluded_fields = ['password_hash','password']
	user 			= User.query.filter_by(id=user_id).first()
	for key in keys:
		if key not in excluded_fields:
			setattr(user, key, data[key])

	db.session.commit()


# This method returns the private id from the uuid
def uuid_to_id(uuid):
	user = User.query.filter_by(public_id=uuid).first()
	return user.id