from app.models.Reward import Reward
from app import db

def get_rewards():
	rewards = Reward.query.filter_by(active=True).all()
	response = []
	for reward in rewards:
		response.append(reward.as_dict())
	return response


def get_reward(id):
	reward = Reward.query.filter_by(id=id).filter_by(active=True).first()
	return reward.as_dict()