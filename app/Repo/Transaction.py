from app.models.Transaction import Transaction
from . import User
from app import db 

def get_transactions(user_id,page=1):
	transactions = Transaction.query.filter_by(user_id=user_id).all()
	response = []
	for transaction in transactions:
		response.append(transaction.as_dict())

	return response

def add_transaction(user_id, data):
	other_id = data['other_id'] if 'other_id' in data else 0

	if other_id != 0:
		other_id = User.uuid_to_id(other_id)

	transaction = Transaction(user_id=user_id, other_id=other_id, txn_type=data['transaction_type'])
	db.session.add(transaction)
	db.session.commit()

	return True