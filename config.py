import os
basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    ADMIN                           = os.environ.get('WA_ADMIN')
    ALLOWED_EXTENSIONS              = set(['jpg', 'png', 'gif'])
    APP_NAME                        = 'lili'
    COMMISSION_FEE                  = 0
    DEBUG                           = False
    JWT_SECRET_KEY                  = "secret-%dfkhdfij-st)ff"
    SECRET_KEY                      = os.environ.get('SECRET_KEY') or 'na-only-you-waka-come'
    SQLALCHEMY_COMMIT_ON_TEARDOWN   = True
    SQLALCHEMY_RECORD_QUERIES       = True
    SLOW_DB_QUERY_TIME              = 0.5
    SSL_DISABLE                     = False
    SQLALCHEMY_TRACK_MODIFICATIONS  = False
    TRANSACTIONS_PER_PAGE           = 10
    UPLOAD_FOLDER                   = os.path.join(basedir, 'user_uploads')
    EXCLUDED_USER_FIELDS            = ['password', 'password_hash']


class Development(Config):
    DEBUG                   = True
    SECRET_KEY              = 'development'
    MAIL_SERVER             = ''
    MAIL_PORT               = 465
    MAIL_USE_TLS            = False
    MAIL_USE_SSL            = True
    MAIL_USERNAME           = ""
    MAIL_SUPPRESS_SEND      = False
    MAIL_PASSWORD           = ""
            # 'sqlite:///' + os.path.join(basedir, 'data-dev.sqlite')


class Production(Config):
    SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://root:jamdat@localhost:3306/lili2' 

config = {
    'development': Development,
    'production': Production,

    'default': Development
}
